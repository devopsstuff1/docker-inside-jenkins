#!/bin/bash

set -o errexit

GREEN='\033[00;32m'
BLUE='\033[00;34m'
RED='\033[00;31m'
NC='\033[0m'

p=$(realpath $0)
dir=$(dirname $p)

set_gid() {
	printf "${BLUE}gid is not specified, do you want to use default gid specified in Dockerfile (y/n)${NC} "
	read answer
	if [ "$answer" = "y" ] || [ "$answer" = "Y" ] ; then
		return 0
	fi

	printf "${BLUE}do you want to use docker gid of host (y/n)${NC} "
	read answer
	if [ "$answer" = "y" ] || [ "$answer" = "Y" ] ; then
		GID="$(grep docker /etc/group | cut -d: -f 3)"
		if [ -z "$GID" ] ; then
			printf "${RED}docker group did not found in /etc/group${NC}\n"
			printf "${RED}canceling${NC}\n"
			exit 1
		fi
		printf "${GREEN}gid $GID will be used${NC}\n"
		return 0
	fi

	printf "${RED}canceling${NC}\n"
	exit 1
}

if (( $# )) ; then
	GID="$1"
	printf "${BLUE}docker gid $GID will be used${NC}\n"
else
	set_gid
fi

if [ -z "$GID" ] ; then
	docker build --build-arg "DOCKER_GID=$GID" -t "asddsajpg/jenkins-with-docker:latest" "$dir/jenkins-master"

	docker build --build-arg "DOCKER_GID=$GID" -t "asddsajpg/jenkins-agent-with-docker:latest" "$dir/jenkins-agent"
else
	docker build -t "asddsajpg/jenkins-with-docker:latest" "$dir/jenkins-master"

	docker build -t "asddsajpg/jenkins-agent-with-docker:latest" "$dir/jenkins-agent"
fi


