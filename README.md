# About
This is a way of deploying Jenkins inside docker and containers can communicate with docker daemon on host

# Usage
## Interactive
```console
./build
```
## Manual
Specify the gid of docker on host
```console
./build <gid>
```

# Hint
You can find gid of docker group by using
```console
grep docker /etc/group | cut -d: -f 3
```
